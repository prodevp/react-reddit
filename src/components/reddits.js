import React, { Component } from 'react';
import {getSubredditNews} from '../services';
import Notifications from 'react-notify-toast';
class Reddits extends Component {
    state ={
        subreddits:[]
    }
    componentWillMount(){
       getSubredditNews(this.props.match.params.subreddit).then((re)=>{
           this.setState({subreddits:re.data.data.children})
       })
           
    }
     
    render() {
        return ( <div className="container">
        <Notifications />
        <h2>Subreddits {this.props.match.params.subreddit}</h2>
       <ul className="list-group">
        { this.state.subreddits.map(function(item,key){
            return(
              <li className="list-group-item" key={key}>
             <a href={item.data.url}>{item.data.title}</a></li>
             
            )  
           
          }) }
        </ul> 
      </div>)
    
        
      }
}
export default Reddits;

    