import React, { Component } from 'react';

class Home extends Component {

  componentWillMount(){
    if(localStorage.getItem('token')){
      this.props.history.push('/news');
    }
  }
  render() {
    return (
        <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <h1 className="App-title">Welcome to Reddit App</h1>
          <a href="https://www.reddit.com/api/v1/authorize?client_id=-3e3mkZMc4JqRQ&response_type=code&state=xxqq&redirect_uri=http://localhost:3000/callback&duration=permanent&scope=save,modposts,identity,edit,flair,history,modconfig,modflair,modlog,modposts,modwiki,mysubreddits,privatemessages,read,report,submit,subscribe,vote,wikiedit,wikiread"><img src="http://groupsrc.com/rdrafts/login_reddit.png?ver=3.0" className="connect-logo" alt="logo" /></a>
        </header>

      </div>
    );
  }

}

export default Home;
