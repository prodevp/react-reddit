import React, { Component } from 'react';
import {getNews , getSubreddit} from '../services';
import {
    Link
  } from 'react-router-dom'
let self;
class News extends Component {

    state ={
        news:[],
        hasSubs:false
    }

    componentWillMount(){
        self = this;
        getSubreddit().then((res)=>{
            console.log(res)
            if(res.data.data.children.length > 0){
                
                this.setState({news:res.data.data.children,hasSubs:true})

            }
            else{
                getNews().then((res)=>{
                    this.setState({news:res.data.data.children,hasSubs:false})
                })
            }
        })
       
      }

  render() {
    return (
        <div className="container">
        <h2>News </h2>
        <ul className="list-group">
        { this.state.news.map(function(item,key){
            return(
              <li className="list-group-item" key={key}>
              {!self.state.hasSubs?<a href={item.data.url}>{item.data.title}</a>:<Link to={'/reddit'+item.data.url}>{item.data.title}</Link>}
              
              </li>
             
            )  
           
          }) }
        </ul>
      </div>
        
    );
  }

}

export default News;