import React, { Component } from 'react';
import axios from 'axios';
class Callback extends Component {

    componentWillMount(){
        if(this.props){
           const code = this.getParameterByName('code',this.props.location.search);
         
          let data = new FormData();
          data.append('code', code);
          data.append('grant_type', 'authorization_code');
          data.append('redirect_uri', 'http://localhost:3000/callback');
          axios('https://www.reddit.com/api/v1/access_token', {
            method: 'POST',
            auth: {
              username: '-3e3mkZMc4JqRQ',
              password: 'u2S0wv6_kn-7J55mEwHQ750mnJQ',
            },
            data: data
          }).then((res)=>{
            if(res.data.access_token){
              localStorage.setItem('token',res.data.access_token);
              this.props.history.push('/');
            }
              
          })  
        }
      }
    
      getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
  render() {
    return (
        <div className="App">
        
      </div>
    );
  }

}

export default Callback;
