import React, { Component } from 'react';
import './App.css';
import {
  Router,
  Route,
  Link
} from 'react-router-dom'
import Home from './components/home'
import Callback from './components/callback'
import News from './components/news'
import Reddits from './components/reddits'
import history from './history'

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      checkLogin:false
    }
    this.userLogout = this.userLogout.bind(this)
  }
 

  componentWillMount(){
    if(localStorage.getItem('token')){
      this.setState({checkLogin:true})
    }
  }

  componentDidUpdate(){
    console.log('update');
    if(localStorage.getItem('token')){
      this.setState({checkLogin:true})
    }
  }

  userLogout(){
    localStorage.removeItem('token')
    this.setState({checkLogin:false})
    history.push('/');
  }
 
  render() {
    console.log(localStorage.getItem('token'))
    return (
      <Router history={history}>
      <div className="container">
      <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a className="navbar-brand">Reddit</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
          <Link className="nav-link" to="/">Home</Link>
          </li>
          {this.state.checkLogin?<li className="nav-item">
          <a className="nav-link" onClick={this.userLogout.bind(this)}>Logout</a>
          </li>:''}
          
        </ul>
      </div>
    </nav>
  
    <main className="container content">
    
        <Route exact path="/" component={Home}/>
        <Route path="/news" component={News}/>
        <Route path="/callback" component={Callback}/>
        <Route path="/reddit/:id/:subreddit" component={Reddits}/>                       
        </main>
      </div>
    </Router>
      
    );
  }

}

export default App;
