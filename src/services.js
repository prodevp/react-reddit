import axios from 'axios';

export function getNews(){
    return axios.get('https://www.reddit.com/r/news.json');
}
export function getSubredditNews(reddit){
    return axios.get('https://www.reddit.com/r/'+reddit+'.json');
}

export function getSubreddit(){
    return axios.get('https://oauth.reddit.com/subreddits/mine/subscriber.json',{headers:{'Authorization':'Bearer '+localStorage.getItem('token')}});

}